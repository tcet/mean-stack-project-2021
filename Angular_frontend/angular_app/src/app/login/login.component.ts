import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form = new FormGroup({
    username: new FormControl(),
    pwd: new FormControl(),
  })
  
  constructor(private service: LoginService) { }

  ngOnInit(): void {
  }

  loginCheck(){
    this.service. postLogin(this.form.value).subscribe((response:any)=>{
      if(response.status == false){
        alert(response.message)
        return false
      }

      localStorage.setItem('jwttoken',response.token)
      window.location.href = "/user"

    })
  }

}
