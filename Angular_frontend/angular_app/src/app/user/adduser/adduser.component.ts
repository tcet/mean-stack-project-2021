import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { UserService } from '../user.service';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {
  form = new FormGroup({
    name: new FormControl(),
    age: new FormControl(),
    city: new FormControl(),
  })

  addeduser:any

  constructor(private service: UserService) { }

  ngOnInit(): void {
  }

  submitUser(){
    //console.log(this.form.value.name);
    this.service.postUser(this.form.value).subscribe((response)=>{
      this.addeduser = response;
      if(!this.addeduser){
        alert("Something went wrong!")
        return false;
      } 
      
      alert(`${this.addeduser.name} was added sucessfully`);
      
      window.location.href = "/user";
    })

  }

}
