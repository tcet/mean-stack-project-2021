import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.css']
})
export class EdituserComponent implements OnInit {

  form = new FormGroup({
    id: new FormControl(),
    name: new FormControl(),
    age: new FormControl(),
    city: new FormControl(),
  })
  id:String;
  u_rec:any;

  constructor(private service:UserService ,private route:ActivatedRoute) { 
    this.id = route.snapshot.params.id
  }

  ngOnInit(): void {
    this.service.getUser(this.id).subscribe((response)=>{
      let data = response[0];
      this.form.patchValue({
        id: data._id,
        name: data.name,
        age: data.age,
        city: data.city,
      })
    })
  }

  editUser(){
    this.service.putUser(this.form.value).subscribe((response)=>{
      this.u_rec= response
      if(!this.u_rec){
        alert(`something went wrong`)
        return false
      }
      alert("user information is update")
      window.location.href = "/user"
    })
  }
}
